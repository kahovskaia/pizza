<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/pizza', [\App\Http\Controllers\PizzaController::class, 'showAll']);
Route::post('/cart', [\App\Http\Controllers\CartController::class, 'addItem']);
Route::get('/cart', [\App\Http\Controllers\CartController::class, 'showCart']);
Route::patch('/cart-item/{id}', [\App\Http\Controllers\CartItemController::class, 'updateItem']);
Route::get('/cart-item/{id}', [\App\Http\Controllers\CartItemController::class, 'showItem']);
