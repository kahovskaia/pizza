<?php

namespace Database\Seeders;

use App\Models\Ingredient;
use App\Models\Pizza;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // create without time stamp, because this seed need for test
        Pizza::insert([
            [
                'title' => 'MacDac Pizza',
                'description' => 'Mmm yum yum',
                'status' => 1,
            ],
            [
                'title' => 'Lovely Mushroom Pizza',
                'description' => 'Has a layer of mushrooms on top',
                'status' => 1,
            ]
        ]);

        Ingredient::insert([
            [
                'title'     => 'Tomato',
                'price'     => 50,
                'status'    => 1,
            ],
            [
                'title'     => 'Sliced mushrooms',
                'price'     => 50,
                'status'    => 1,
            ],
            [
                'title'     => 'Feta cheese',
                'price'     => 100,
                'status'    => 1,
            ],
            [
                'title'     => 'Sausages',
                'price'     => 100,
                'status'    => 1,
            ],
            [
                'title'     => 'Sliced onion',
                'price'     => 50,
                'status'    => 1,
            ],
            [
                'title'     => 'Mozzarella cheese',
                'price'     => 30,
                'status'    => 1,
            ],
            [
                'title'     => 'Oregano',
                'price'     => 200,
                'status'    => 1,
            ],
            [
                'title'     => 'Bacon',
                'price'     => 100,
                'status'    => 1,
            ],
        ]);

        $pizzas = Pizza::all();

        $pizzas[0]->ingredients()->attach([1, 2, 3, 4, 6, 7]);
        $pizzas[1]->ingredients()->attach([1, 8, 6, 7]);
    }
}
