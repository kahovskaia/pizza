<?php

namespace App\Traits;

use Illuminate\Support\Facades\Response;

trait HasJsonResponse
{
    /**
     * @param $result
     * @param $message
     * @param $code
     * @return mixed
     */
    public function sendResponse($result, $code) {
        return Response::json(self::makeResponse($result), $code);
    }

    /**
     * @param $error
     * @param int $code
     * @param array $data
     * @return mixed
     */
    public function sendError($error, $code = 400, $data = []) {
        return Response::json(self::makeError($error, $data), $code);
    }

    /**
     * @param string $message
     * @param mixed  $data
     *
     * @return array
     */
    public static function makeResponse($data)
    {
        return [
            'success' => true,
            'data'    => $data,
        ];
    }

    /**
     * @param string $message
     * @param array  $data
     *
     * @return array
     */
    public static function makeError($message, $data)
    {
        return  [
            'success' => false,
            'data' => $data,
            'message' => $message,
        ];
    }
}

