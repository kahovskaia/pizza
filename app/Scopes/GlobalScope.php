<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class GlobalScope implements Scope
{
    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 0;

    public function apply(Builder $builder, Model $model)
    {
        $builder->where('status', self::STATUS_ENABLE);
    }
}
