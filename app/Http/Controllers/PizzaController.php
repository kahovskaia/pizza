<?php

namespace App\Http\Controllers;

use App\Services\PizzaService;
use Illuminate\Http\Request;

class PizzaController extends Controller
{
    public PizzaService $pizzaService;

    public function __construct(PizzaService $pizzaService){
        $this->pizzaService = $pizzaService;
    }

    public function showAll(){
        $data = $this->pizzaService->showAll();

        return $this->sendResponse($data, 200);
    }
}
