<?php

namespace App\Http\Controllers;

use App\Http\Requests\CartAddItemRequest;
use App\Services\CartService;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public CartService $cartService;

    public function __construct(CartService $cartService){
        $this->cartService = $cartService;
    }

    public function addItem(CartAddItemRequest $request){
        $result = $this->cartService->addItem($request->validated());

        return $this->sendResponse($result, 200);
    }

    public function showCart(){
        $result = $this->cartService->showCart();

        return $this->sendResponse($result, 200);
    }
}
