<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\CartItem;
use App\Services\CartItemService;
use Illuminate\Http\Request;

class CartItemController extends Controller
{
    public CartItemService $cartItemService;

    public function __construct(CartItemService $cartItemService){
        $this->cartItemService = $cartItemService;
    }

    public function showItem($id, Request $request){
        $result = $this->cartItemService->showItem($request, $id);

        return $this->sendResponse($result, 200);
    }

    public function updateItem($id, Request $request){
        $result = $this->cartItemService->updateItem($request, $id);

        return $this->sendResponse($result, 200);
    }
}
