<?php

namespace App\Http\Requests;

use App\Models\Ingredient;
use App\Models\Pizza;
use Illuminate\Validation\Rule;

class CartAddItemRequest extends FormRequest
{

    public function rules()
    {
        return [
            'pizza_id' => [
                'required',
                Rule::exists('pizzas', 'id')->where(function ($query) {
                    $query->where('id', $this->request->get('pizza_id'))->where('status', 1);
                })
            ],
            'variation_ids' => 'required|array',
            'variation_ids.*' => [
                'required',
                'exists:' . Ingredient::class . ',id'
            ]
        ];
    }
}
