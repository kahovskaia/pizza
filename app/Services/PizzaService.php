<?php

namespace App\Services;

use App\Models\Pizza;

class PizzaService
{
    public Pizza $pizza;

    public function __construct(Pizza $pizza)
    {
        $this->pizza = $pizza;
    }

    public function showAll(){
        $pizzas = $this->pizza
            ->newQuery()
            ->with('ingredients', function ($query){
                return $query->where('status', 1);
            })
            ->get();

        return $pizzas;
    }
}
