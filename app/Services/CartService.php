<?php

namespace App\Services;



use App\Models\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CartService
{
    public $cart;

    public function __construct(Request $request){
        $this->cart = $this->setCart($request);
    }

    public function addItem($data){
        if (!$this->cart){
            $this->cart =  $this->createCart();
        }

        $response = $this->cart->cartItems()->create([
            'pizza_id'      => $data['pizza_id'],
            'variation_ids' => json_encode($data['variation_ids']),
        ]);

        return $response->cart;
    }

    public function showCart(){
        if (!isset($this->cart)){
            return [];
        }

        $cartItems = $this->cart->cartItems()->with('pizza.ingredients')->get()->toArray();

        foreach ($cartItems as $keyCartItem => $item){
            $variation_ids = $item['variation_ids'];

            $ingrs = $item['pizza']['ingredients'];
            foreach ($ingrs as $keyIngr => $ingr){
                if(gettype(array_search($ingr['id'], $variation_ids)) !== 'integer'){
                    unset($cartItems[$keyCartItem]['pizza']['ingredients'][$keyIngr]);
                }
            }
        }

        return ['items' => $cartItems, 'cart' => $this->cart];
    }

    public function setCart(Request $request){
        $existCart = Cart::where('token', $request->cookies->get('token'))->first();

        return $existCart;
    }

    public function createCart(){
        $cart = new Cart();
        $rand = Str::random(10);

        return $cart->create([
           'token' => $rand,
           'price' => 0
        ]);
    }
}
