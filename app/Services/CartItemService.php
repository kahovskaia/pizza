<?php

namespace App\Services;

use App\Models\CartItem;
use Illuminate\Http\Request;

class CartItemService
{

    public function __construct()
    {
    }


    public function showItem(Request $request, $id){
        $this->accessItem($request, $id);
        $item = CartItem::where('id', $id)->with('pizza.ingredients')->first();

        return $item;
    }

    public function updateItem(Request $request, $id){
        $this->accessItem($request, $id);
        $item = CartItem::where('id', $id)->with('pizza.ingredients')->first();
        $item->update([
            'variation_ids' => json_encode($request->get('variation_ids'))
        ]);
        return $item;
    }

    private function accessItem(Request $request, $id){
        $cartService = new CartService($request);
        $cart = $cartService->cart;

        if (isset($cart) && $cart->cartItems->where('id', $id)->first() == true){
            return true;
        }else{
            throw new \Exception('Forbidden', 403);
        }

    }
}
