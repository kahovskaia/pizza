<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    use HasFactory;

    protected $fillable = [
        'pizza_id',
        'variation_ids',
        'price',
    ];

    public function cart(){
        return $this->belongsTo(Cart::class);
    }

    public function pizza(){
        return $this->belongsTo(Pizza::class);
    }

    public function getVariationIdsAttribute($value) {
        return json_decode($value);
    }

    public function itemPriceCount(){
        $ingrIds = $this->variation_ids;
        $ingrCost = Ingredient::whereIn('id', $ingrIds)->sum('price');
        $totalPizzaPrice = $ingrCost / 2 + $ingrCost;
        $this->price = $totalPizzaPrice;
    }
}
