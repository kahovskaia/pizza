<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;

    protected $fillable = [
        'token',
        'price'
    ];

    public function cartItems(){
        return $this->hasMany(CartItem::class);
    }

    public function totalPriceCount(){
        $totalPrice = 0;
        foreach ($this->cartItems as $item){
            $totalPrice += $item->price;
        }

        if ($totalPrice > 0){
            $this->update(['price' => $totalPrice]);
        }else{
            $this->delete();
        }
    }
}
