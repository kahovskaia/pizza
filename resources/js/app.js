import router from "./router";

require('./bootstrap');

// es2015 module
import cookies from 'vue-cookies'

Vue.use(cookies, { expire: '7d'})
import Vue from 'vue'

import App from  './layouts/App'

const app = new Vue({
    router,
    el: '#app',
    components: { App },
})
