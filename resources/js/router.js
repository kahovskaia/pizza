import Router from 'vue-router'
import Vue from "vue";

Vue.use(Router)

/*
   Make sure your pages components are inside 'resources/assets/js/pages' folder
*/
const Pizza = require('./pages/pizza.vue').default
const Cart = require('./pages/cart.vue').default
const CartItem = require('./pages/cartItem.vue').default

let router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'pizza',
            component: Pizza
        },
        {
            path: '/cart',
            name: 'cart',
            component: Cart
        },
        {
            path: '/cart-item/:id',
            name: 'cartItem',
            component: CartItem
        }
    ]
})

export default router
